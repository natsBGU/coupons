﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public enum Category
    {
       Food, 
       Sport, 
       Electronics, 
       Cinema, 
       HomeAndOffice, 
       Outdoors, 
       Fashion, 
       Toys, 
       Other, 
    };
}
