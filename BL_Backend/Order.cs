﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * CREATE TABLE Orders(
serial varchar(50) primary key,
orderDate date not null,
isUsed varchar(50) not null
); */


namespace BL_Backend
{
    public class Order
    {
        private string _serial;
        public string serial
        {
            get { return _serial; }
            set { _serial = value; }
        }
        private DateTime _orderDate;
        public DateTime orderDate
        {
            get { return _orderDate; }
            set { _orderDate = value; }
        }
        private int _quantity;
        public int quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        private string _username;
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }
        private string _couponName;
        public string couponName
        {
            get { return _couponName; }
            set { _couponName = value; }
        }
        private bool _isUsed;
        public bool isUsed
        {
            get { return _isUsed; }
            set { _isUsed = value; }
        }
        public Order(string serial, DateTime orderDate, int quantity, string username, string couponName, bool isUsed)
        {
            this.serial = serial;
            this.orderDate = orderDate;
            this.quantity = quantity;
            this.username = username;
            this.couponName = couponName;
            this.isUsed = isUsed;
        }
    }
}
