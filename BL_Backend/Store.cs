﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Store : Users
    {
        private string _ownerName;
        public string ownerName
        {
            get { return _ownerName; }
            set { _ownerName = value; }
        }
        private string _address;
        public string address
        {
            get { return _address; }
            set { _address = value; }
        }
        private string _city;
        public string city
        {
            get { return _city; }
            set { _city = value; }
        }
        private string _description;
        public string description
        {
            get { return _description; }
            set { _description = value; }
        }
        private Category _category;
        public Category category
        {
            get { return _category; }
            set { _category = value; }
        }
        private string _storeName;
        public string storeName
        {
            get { return _storeName; }
            set { _storeName = value; }
        }

        public Store(string username, string ownerName, string address, string city, string description, Category category, string storeName, string password, string phone, string mail)
            : base(username, password, phone, mail, 1)
        {
            this.ownerName = ownerName;
            this.address = address;
            this.city = city;
            this.description = description;
            this.category = category;
            this.storeName = storeName;
        }
    }
}
