﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Checker// initial fileds
    {
        public bool checkLength(string s, int num)// checks the right length of a string
        {
            if (s != null) { return (s.Length == num); }
            return false;

        }

        public bool checkOnlyDigitis(string s)// checks if a string contains only digits
        {
            foreach (char c in s)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        public bool checkOnlyLetters(string s)// checks if a string contanis only letters
        {
            for (int i = 1; i < s.Length; i++)
            {
                if (!(s[i] >= 'a' && s[i] <= 'z'))
                {
                    return false;
                }
            }
            return true;
        }

        public bool checkUppercase(string s)// checks that the first letter is a uppercase 
        {
            if (!(s[0] >= 'A' && s[0] <= 'Z' || s[0] == '-'))
            {
                return false;
            }
            return true;
        }
        public bool checkDate(string s)
        {
            bool date = true;
            if (s.Length != 10)
                date = false;
            if (s[2] != '.' && s[5] != '.')
                date = false;
            for (int i = 0; date == true && i < 7; i = i + 3)
            {
                if (s[i] < '0' || s[i] > '9' || s[i + 1] < '0' || s[i + 1] > '9')
                    date = false;
            }
            if (s[8] < '0' || s[8] > '9' || s[9] < '0' || s[9] > '9')
                date = false;


            return date;
        }
    }
}
