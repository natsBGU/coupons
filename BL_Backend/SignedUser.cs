﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class SignedUser: Users
    {
        private string _lastLocation;
        public string lastLocation
        {
            get { return _lastLocation; }
            set { _lastLocation = value; }
        }
        private List<Category> _categories;
        public List<Category> categories
        {
            get { return _categories; }
            set { _categories = value; }
        }

        public SignedUser(string username, string lastLocation, string password, string phone, string mail)
            : base(username, password, phone, mail, 2)
        {
            this.lastLocation = GPS.getLocation();
            _categories = new List<Category>();
        }

        public void addCategory(Category c)
        {
            _categories.Add(c);
        }

    }
}
