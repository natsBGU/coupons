﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BL_Backend
{
    public class Users
    {
        public Checker c = new Checker();
        private string _username;
        public string username
        {
            get { return _username; }
            set{ _username = value;}
        }
        private string _password;
        public string password
        {
            get { return _password; }
            set { _password = value; }
        }
        private string _phone;
        public string phone
        {
            get { return _phone; }
            set { this._phone = value; }
        }
        private string _mail;
        public string mail
        {
            get { return _mail; }
            set { this._mail = value; }
        }
        private int _clearence;
        public int clearence
        {
            get { return _clearence; }
            set{ this._clearence = value; }
        }


        public Users(string username, string password, string phone, string mail, int clearence)
        {
            this.username = username;
            this.password = password;
            this.phone = phone;
            this.mail = mail;
            this.clearence = clearence;
        }
    }

}
