﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
namespace BL_Backend
{
    public class GPS 
    {

        public GPS(){}

        public static string getLocation()
        {
            Random rand = new Random();
            int loc = rand.Next(12);
            switch(loc) {
                case 1 :
                    return "Tel Aviv";
                case 2:
                    return "Haifa";
                case 3:
                    return "Tzfat";
                case 4:
                    return "Hod-Hasaron";
                case 5:
                    return "Jerusalem";
                case 6:
                    return "Beer Sheva";
                case 7:
                    return "Dgania A";
                case 8:
                    return "Eilat";
                case 9:
                    return "Asdhod";
                case 10:
                    return "Hadera";
                case 11:
                    return "New York";
                default:
                    return "Netanya";
            }
        }
    }
}
