﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 * CREATE TABLE Social(
username varchar(50) foreign key REFERENCES Users(username),
networkname varchar(50) not null,
CONSTRAINT pksocial PRIMARY KEY (username, networkname)
); */


namespace BL_Backend
{
    public class Social
    {
        private string _username;
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }
        private string _networkname;
        public string networkname
        {
            get { return _networkname; }
            set { _networkname = value; }
        }
        public Social(String username, String networkname)
        {
            this.username = username;
            this.networkname = networkname;
        }
    }
}
