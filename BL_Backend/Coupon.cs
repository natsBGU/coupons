﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Coupon
    {
        public Checker c = new Checker();
        private String _name;
        public String name
        {
            get { return _name; }
            set { _name = value; }
        }
        private String _description;
        public String description
        {
            get { return _description; }
            set { _description = value; }
        }
        private int _price;
        public int price
        {
            get { return _price; }
            set { _price = value; }
        }
        private int _priceAfterDiscount;
        public int priceAfterDiscount
        {
            get { return _priceAfterDiscount; }
            set { _priceAfterDiscount = value; }
        }
        private int _rate;
        public int rate
        {
            get { return _rate; }
            set { _rate = value; }
        }
        private int _numOfRaters;
        public int numOfRaters
        {
            get { return _numOfRaters; }
            set { _numOfRaters = value; }
        }
        private Category _category;
        public Category category
        {
            get { return _category; }
            set { _category = value; }
        }
        private DateTime _expiration;
        public DateTime expiration
        {
            get { return _expiration; }
            set { _expiration = value; }
        }
        private string _storeName;
        public string storeName
        {
            get { return _storeName; }
            set { _storeName = value; }
        }
        private bool _approved;
        public bool approved
        {
            get { return _approved; }
            set { _approved = value; }
        }


        public Coupon(string name, string description, int price, int priceAfterDiscount, int rate, int numOfRaters, Category category, DateTime expiration, string storeName, int approved)
        {
            _name = name;
            _description = description;
            _price = price;
            _priceAfterDiscount = priceAfterDiscount;
            _rate = rate;
            _numOfRaters = numOfRaters;
            _category = category;
            _expiration = expiration;
            _storeName = storeName;
            _approved = (approved==0) ? false : true;
        }

    }
    
}
