﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class UserControllerImp : UserController
    {
        public IDAL dal;

        public UserControllerImp(IDAL dal)
        {
            this.dal = dal;
        }

        public List<Coupon> searchCoupon(string name, string city, List<Category> cat)
        {
            return dal.getCouponsByStoreNameCityAndCategories(name, city, cat);
        }

        public List<Store> searchStore(string name, string city, List<Category> cat)
        {
            return dal.getStoresByStoreNameCityAndCategories(name, city, cat);
        }

        public List<Order> myCoupons(string username)
        {
            if (username != null)
            {
                return dal.getOrderedCouponsByUsername(username);
            }
            return null;
        }

        public int orderCoupon(Coupon c, int quantity, string username)
        {
            if (c != null && quantity > 0 && username != null)
            {
                bool exist = dal.isUsernameAlreadyExists(username);
                if (exist == true)
                {
                    dal.orderCoupon(c, quantity, username);
                    return 1;
                }
                return -2;
            }
            return -1;
        }


        public void rateCoupon(string couponName, int rate)
        {
            if (couponName != null && rate >= 0 && rate <= 5)
            {
                Coupon c = getCouponByName(couponName);
                if (c != null)
                    dal.rateCoupon(c, rate);
            }
        }

        public Coupon getCouponByName(string name)
        {
            return dal.getCouponByName(name);
        }

        public Coupon randomCouponHourly(string username)
        {
            throw new NotImplementedException();
        }

        public Coupon warningOfMisUs(string username)
        {
            throw new NotImplementedException();
        }
    }
}
