﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class ManagerControllerImp : ManagerController
    {
        public IDAL dal;

        public ManagerControllerImp(IDAL dal)
        {
            this.dal = dal;
        }

        public List<Coupon> getAllCoupons()
        {
            return dal.getAllCoupons();
        }

        public bool editCoupon(string couponName, Coupon newCoupon)
        {
            if (couponName != null && newCoupon != null)
            {
                dal.editCoupon(couponName, newCoupon);
                return true;
            }
            return false;
        }

        public List<Manager> getAllManagers()
        {
            return dal.getAllManagers();
        }

        public bool removeStore(string username)
        {
            if (username != null)
            {
                Store s = dal.getStoreByUsername(username);
                if (s != null)
                {
                    dal.removeStore(username);
                    return true;
                }
            }
            return false;
        }

        public bool addCoupon(Coupon c)
        {
            if (c != null && !dal.isCouponAlreadyExists(c.name))
            {
                dal.addCoupon(c);
                return true;
            }
            return false;
        }

        public bool addStore(Store s)
        {
            if (s != null && !dal.isUsernameAlreadyExists(s.username))
            {
                dal.addStore(s);
                return true;
            }
            return false;
        }

        public bool couponExist(string couponName)
        {
            if (couponName != null)
            {
                return dal.isCouponAlreadyExists(couponName);
            }
            return false;
        }

        public bool editManager(string username, Manager newManager)
        {
            if (username != null && newManager != null)
            {
                if (dal.isUsernameAlreadyExists(username))
                {
                    dal.editManager(username, newManager);
                    return true;
                }

            }
            return false;
        }

        public void rateCoupon(string couponName, int rate)
        {
            if (couponName != null && rate >= 0 && rate <= 5)
            {
                Coupon c = getCouponByName(couponName);
                if (c != null)
                    dal.rateCoupon(c, rate);
            }
        }

        public Coupon getCouponByName(string name)
        {
            return dal.getCouponByName(name);
        }

        public bool removeCoupon(string couponName)
        {
            if (couponName != null)
            {
                Coupon c = dal.getCouponByName(couponName);
                if (c != null)
                {
                    dal.removeCoupon(couponName);
                    return true;
                }
            }
            return false;
        }

        public bool usernameExist(string username)
        {
            if (username != null)
            {
                return dal.isUsernameAlreadyExists(username);
            }
            return false;
        }

        public bool addManager(Manager s)
        {
            if (s != null && !dal.isUsernameAlreadyExists(s.username))
            {
                dal.addManager(s.username, s.password, s.phone, s.mail);
                return true;
            }
            return false;
        }

        public bool removeManager(string username)
        {
            if (username != null)
            {
                Manager m = dal.getManagerByUsername(username);
                if (m != null)
                {
                    dal.removeManagerByUsername(username);
                    return true;
                }
            }
            return false;
        }

        public Manager GetManagerByUsername(string username)
        {
            if (username != null)
            {
                if (dal.isUsernameAlreadyExists(username))
                {
                    return dal.getManagerByUsername(username);
                }
                else
                    return null;
            }
            else return null;
        }


        public bool removeSignedUser(string username)
        {
            if (username != null)
            {
                SignedUser s = dal.getSignedUserByUsername(username);
                if (s != null)
                {
                    dal.removeSignedUserByUsername(username);
                    return true;
                }
            }
            return false;
        }

        public bool editStore(string username, Store newStore)
        {
            if (username != null && newStore != null)
            {
                if (dal.isUsernameAlreadyExists(username))
                {
                    dal.editStoreDetails(username, newStore);
                    return true;
                }

            }
            return false;
        }

        public Store GetStoreByUsername(string username)
        {
            if (username != null)
            {
                if (dal.isUsernameAlreadyExists(username))
                {
                    return dal.getStoreByUsername(username);
                }
                else return null;
            }
            else return null;
        }
    }
}
