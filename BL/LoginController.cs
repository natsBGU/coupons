﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace BL
{
    public interface LoginController
    {
        bool usernameExist(string username);
        bool addSignedUser(SignedUser s);
        SignedUser GetSignedUserByUsername(string username);
        int login(string username, string password);
        string getSignUserEmail(string username);
        string getSignUserPassword(string username);
    }
}
