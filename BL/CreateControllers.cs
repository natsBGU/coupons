﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class CreateControllers
    {
        UserController uc;
        LoginController lc;
        StoreController sc;
        ManagerController mc;

        public CreateControllers(IDAL dal)
        {
            uc = new UserControllerImp(dal);
            lc = new LoginControllerImp(dal);
            sc = new StoreControllerImp(dal);
            mc = new ManagerControllerImp(dal);
        }

        public UserController getUserController()
        {
            return uc;
        }

        public LoginController getLoginController()
        {
            return lc;
        }

        public StoreController getStoreController()
        {
            return sc;
        }

        public ManagerController getManagerController()
        {
            return mc;
        }
    }
}
