﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class LoginControllerImp : LoginController
    {
        public IDAL dal;

        public LoginControllerImp(IDAL dal)
        {
            this.dal = dal;
        }

        public bool usernameExist(string username)
        {
            if (username != null)
            {
                return dal.isUsernameAlreadyExists(username);
            }
            return false;
        }

        public bool addSignedUser(SignedUser s)
        {
            if (s != null && !dal.isUsernameAlreadyExists(s.username))
            {
                dal.addUser(s.username, s.password, s.phone, s.mail, s.categories);
                return true;
            }
            return false;
        }

        public SignedUser GetSignedUserByUsername(string username)
        {
            if (username != null)
            {
                if (dal.isUsernameAlreadyExists(username))
                {
                    return dal.getSignedUserByUsername(username);
                }
                else return null;
            }
            else return null;
        }

        public int login(string username, string password)
        {
            if (username != null && username != "" && password != null && password != "")
            {
                int ans = dal.userLogin(username, password);
                return ans;
            }
            else return -1;
        }

        public string getSignUserEmail(string username) 
        {
            return dal.signUserEmail(username);
        }

        public string getSignUserPassword(string username)
        {
            return dal.getSignedUserByUsername(username).password;
        }
    }
}
