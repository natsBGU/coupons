﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace BL
{
    public interface ManagerController
    {
        List<Coupon> getAllCoupons();
        bool editCoupon(string couponName, Coupon newCoupon);
        List<Manager> getAllManagers();
        Store GetStoreByUsername(string username);
        bool addCoupon(Coupon c);
        bool addStore(Store s);
        bool couponExist(string couponName);
        bool editManager(string username, Manager newManager);
        Coupon getCouponByName(string name);
        bool removeCoupon(string couponName);
        bool usernameExist(string username);
        bool addManager(Manager s);
        bool removeManager(string username);
        Manager GetManagerByUsername(string username);
        bool removeStore(string username);
        bool removeSignedUser(string username);
        bool editStore(string username, Store newStore);
    }
}
