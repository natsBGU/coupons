﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace BL
{
    public interface StoreController
    {
        
          Store GetStoreByUsername(string username);
          List<Order> myStoreOrders(string storename);
          List<Coupon> getStoreCoupons(string storename);
          bool couponExist(string couponName);
          bool addCoupon(Coupon c);
          int useCoupon(string serial);
          List<Coupon> getAllApprovedCoupons();
          List<Coupon> getAllUnapprovedCoupons();
    }
}
