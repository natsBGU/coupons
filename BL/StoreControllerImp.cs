﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public class StoreControllerImp : StoreController
    {
        public IDAL dal;

        public StoreControllerImp(IDAL dal)
        {
            this.dal = dal;
        }

        public Store GetStoreByUsername(string username)
        {
            if (username != null)
            {
                if (dal.isUsernameAlreadyExists(username))
                {
                    return dal.getStoreByUsername(username);
                }
                else return null;
            }
            else return null;
        }

        public List<Order> myStoreOrders(string storename)
        {
            if (storename != null)
                return dal.getAllCouponOrdersFromStore(storename);
            return null;
        }

        public List<Coupon> getStoreCoupons(string storename)
        {
            if (storename != null)
            {
                return dal.getCouponsByStoreName(storename);
            }
            return null;
        }

        public bool couponExist(string couponName)
        {
            if (couponName != null)
            {
                return dal.isCouponAlreadyExists(couponName);
            }
            return false;
        }

        public bool addCoupon(Coupon c)
        {
            if (c != null && !dal.isCouponAlreadyExists(c.name))
            {
                dal.addCoupon(c);
                return true;
            }
            return false;
        }

        public int useCoupon(string serial)
        {
            if (serial != null)
            {
                Order order = dal.getOrderBySerialKey(serial);
                if (order != null)
                {
                    if (dal.fulfillCouponAndApprove(order) == true)
                        return 1;
                    return -2; //coupon already fulfilled
                }
                return -1; //order not found
            }
            return -1; //serial not valid
        }

        public List<Coupon> getAllApprovedCoupons()
        {
            return dal.getAllApprovedCoupons();
        }

        public List<Coupon> getAllUnapprovedCoupons()
        {
            return dal.getAllNotApprovedCoupons();
        }
    }
}
