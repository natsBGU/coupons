﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace BL
{
    public interface UserController
    {
        List<Coupon> searchCoupon(string name, string city, List<Category> cat);
        List<Store> searchStore(string name, string city, List<Category> cat);
        List<Order> myCoupons(string username);
        int orderCoupon(Coupon c, int quantity, string username);
        void rateCoupon(string couponName, int rate);
        Coupon getCouponByName(string name);
    }
}
