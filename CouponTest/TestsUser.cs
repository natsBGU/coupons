﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;
using BL;
using NUnit.Framework;

namespace CouponTest
{
    [TestFixture]
    class TestsUser
    {

        IDAL dal;
        UserController con;
        Coupon c , c1;
        SignedUser u;
        Store s;
        public TestsUser()
        {
            dal = new DAL.DAL();
            con = new UserControllerImp(dal);
            u = new SignedUser("RandomUser", "Haifa", "121212", "089974312", "maxstar112233@gmail.com");
            s = new Store("RandomStore", "bob", "Not", "Haifa", "store for testing", Category.Other, "Random Store", "12", "048899898", "store@gmail.com");
            c = new Coupon("RandomCoupon", "testing cpupon", 10, 5, 0, 0, Category.Other, DateTime.Today, "RandomStore", 0);
            c1 = new Coupon("RandomCoupon1", "testing cpupon", 10, 5, 0, 0, Category.Other, DateTime.Today, "RandomStore", 0);
            dal.removeSignedUserByUsername(u.username);
            dal.removeStore(s.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
        }

        [Test] // 1
        public void order()
        {
            List<Category>  l = new List<Category>();
            l.Add(Category.Outdoors);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addStore(s);
            dal.addUser(u.username , u.password , u.phone , u.mail , l);
            dal.addCoupon(c);

            con.orderCoupon(c, 3, "RandomUser");
            Assert.IsTrue(con.myCoupons("RandomUser").Count > 0);

            dal.removeSignedUserByUsername(u.username);
            dal.removeStore(s.username);
            dal.removeCoupon(c.name);
        }

        [Test] // 2
        public void searchCoupon()
        {
            List<Category> l = new List<Category>();
            l.Add(Category.Outdoors);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addCoupon(c);
            Assert.IsNotNull(con.searchCoupon(null, "Haifa", l));
            dal.removeCoupon(c.name);
        }

        [Test] // 3
        public void searchCouponNotFound()
        {
            List<Category> l = new List<Category>();
            l.Add(Category.Outdoors);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addCoupon(c);
            dal.addCoupon(c1);
            Assert.IsEmpty(con.searchCoupon(null, "Tel Aviv" , l));
            l.Remove(Category.Other);
            Assert.IsEmpty(con.searchCoupon(null, "Haifa", l));
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
        }

        [Test] // 4
        public void myCouponsList()
        {
            List<Category> l = new List<Category>();
            l.Add(Category.Outdoors);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addCoupon(c);
            dal.addCoupon(c1);
            dal.addUser(u.username, u.password, u.phone, u.mail, l);
            con.orderCoupon(c, 3, "RandomUser");
            con.orderCoupon(c1, 3, "RandomUser");
            Assert.IsTrue(con.myCoupons(u.username).Count > 0);
            dal.removeSignedUserByUsername(u.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
        }
    
        [Test] // 5
        public void searchCouponAfterUse()
        {

            dal.addCoupon(c);
            con.rateCoupon(c.name ,5);
            Assert.IsTrue(dal.getCouponByName(c.name).rate ==  5);
            con.rateCoupon(c.name, 3);
            Assert.AreEqual(dal.getCouponByName(c.name).rate, 4);
            dal.removeCoupon(c.name);
        }

        [Test] // 6
        public void numOfRaters()
        {
            dal.addCoupon(c);
            Assert.AreEqual(dal.getCouponByName(c.name).numOfRaters,0);
            con.rateCoupon(c.name ,5);
            con.rateCoupon(c.name ,5);
            con.rateCoupon(c.name ,5);
            con.rateCoupon(c.name ,5);
            Assert.AreEqual(dal.getCouponByName(c.name).numOfRaters,4);
            dal.removeCoupon(c.name);
        }

        [Test] // 7
        public void orderAndRate()
        {
            List<Category> l = new List<Category>();
            l.Add(Category.Outdoors);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addCoupon(c);
            dal.addCoupon(c1);
            dal.addUser(u.username, u.password, u.phone, u.mail, l);
            con.orderCoupon(c, 3, "RandomUser");
            con.orderCoupon(c1, 3, "RandomUser");
            Assert.IsTrue(con.myCoupons(u.username).Count > 0);
            con.rateCoupon(c.name, 5);
            con.rateCoupon(c1.name, 3);
            Assert.AreEqual(dal.getCouponByName(c.name).rate, 5);
            Assert.AreEqual(dal.getCouponByName(c1.name).rate, 3);
            dal.removeSignedUserByUsername(u.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
        }
    }
}
