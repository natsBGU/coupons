﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;
using BL;
using NUnit.Framework;

namespace CouponTest
{
    [TestFixture]
    class TestsLogin
    {

        IDAL dal;
        LoginController con;
        public TestsLogin()
        {
            dal = new DAL.DAL();
            con = new LoginControllerImp(dal);
        }

        [Test] //1
        public void loginStore()
        {
            Assert.AreEqual(con.login("dominos", "1"), 1);
        }

        [Test] //2
        public void loginAdimn()
        {
            Assert.AreEqual(con.login("admin", "1"), 0);
        }

        [Test] //3
        public void loginUser()
        {
            Assert.AreEqual(con.login("noam", "121212"), 2);
        }

        [Test] //4
        public void loginWrongPass()
        {
            Assert.AreEqual(con.login("noam", "12"), -1);
        }

        [Test] //5
        public void loginNoUserName()
        {
            Assert.AreEqual(con.login("", "121212"), -1);
        }

        [Test] //6
        public void loginWrongName()
        {
            Assert.AreEqual(con.login("nonoa", ""), -1);
        }

        [Test] //7
        public void nameEx()
        {
            Assert.IsTrue(con.usernameExist("noam"));
        }

        [Test] //8
        public void nameDontEx()
        {
            Assert.IsFalse(con.usernameExist("Sinderala"));
            SignedUser s = new SignedUser("Sinderala", "Haifa" , "1" , "05050505" , "sind@gmail.com");
            con.addSignedUser(s);
            Assert.IsTrue(con.usernameExist("Sinderala"));
            dal.removeSignedUserByUsername("Sinderala");
            Assert.IsFalse(con.usernameExist("Sinderala"));
        }
    }
}
