﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;
using BL;
using NUnit.Framework;

namespace CouponTest
{
    [TestFixture]
    public class DALTests
    {
        IDAL dal;
        public DALTests()
        {
            dal = new DAL.DAL();
        }

        [Test] //1
        public void addUser()
        {
            List<Category> c = new List<Category>();
            c.Add(Category.Cinema);
            c.Add(Category.Food);
            dal.addUser("GrampelStillSkin2", "121212", "0505050550", "grampel@gmail.com", c);
            Assert.IsTrue(dal.isUsernameAlreadyExists("GrampelStillSkin2"));
            dal.removeSignedUserByUsername("GrampelStillSkin2");
        }

        [Test]  //2
        public void SearchUserByName()
        {
            Assert.IsTrue(dal.getSignedUserByUsername("noam").username == "noam");
        }

        [Test]  //3
        public void removeUser()
        {
            dal.addUser("GrampelStillSkin1", "121212", "0505050550", "grampel@gmail.com", new List<Category>());
            dal.removeSignedUserByUsername("GrampelStilllSkin1");
            Assert.IsFalse(dal.isUsernameAlreadyExists("GrampelStilllSkin1"));
            dal.removeSignedUserByUsername("GrampelStillSkin1");
        }

        [Test]  //4
        public void addManager()
        {
            dal.removeManagerByUsername("Kraing");
            Assert.IsFalse(dal.isUsernameAlreadyExists("Kraing"));
            dal.addManager("Kraing", "121212", "0507889900", "kraing@post.cs.bgu.ac.il");
            Assert.IsTrue(dal.isUsernameAlreadyExists("Kraing"));
            Assert.AreEqual(0 , dal.userLogin("Kraing", "121212"));
            dal.removeManagerByUsername("Kraing");
        }

        [Test] //5
        public void removeManager()
        {
            dal.removeManagerByUsername("Kraing");
            dal.addManager("Kraing", "121212", "0507889900", "kraing@post.cs.bgu.ac.il");
            Assert.AreEqual(0, dal.userLogin("Kraing", "121212"));
            dal.removeManagerByUsername("Kraing");
            Assert.IsFalse(dal.isUsernameAlreadyExists("Kraing"));
        }


        [Test]  //6
        public void addStore()
        {
            dal.removeStore("CentralCofe");
            Assert.IsFalse(dal.isUsernameAlreadyExists("CentralCofe"));
            Store s = new Store("CentralCofe" , "bob" , "Hatikva" , "Haifa" , "a Cofe shop" , Category.Food , "Central Cofe" , "121212" , "049999222", "cofe@mail.com");
            dal.addStore(s);
            Assert.IsTrue(dal.isUsernameAlreadyExists("CentralCofe"));
            dal.removeStore(s.username);
            Assert.IsFalse(dal.isUsernameAlreadyExists("CentralCofe"));
        }

        [Test] //7
        public void addCouponToStore()
        {
            dal.removeCoupon("FreeCofe");
            Coupon c = new Coupon("FreeCofe" , "a free cofe just for over 25 years old" , 8 ,0 , 5 , 10 , Category.Food , DateTime.Now , "Central Cofe" , 0);
            dal.addCoupon(c);
            Assert.IsTrue( dal.getCouponByName("FreeCofe").priceAfterDiscount == 0);
            dal.removeCoupon("FreeCofe");
            Assert.IsTrue(dal.getCouponByName("FreeCofe") == null);
        }

        [Test] //8
        public void apprveCoupon()
        {
            dal.removeCoupon("FreeCofe");
            Coupon c = new Coupon("FreeCofe", "a free cofe just for over 25 years old", 8, 0, 5, 10, Category.Food, DateTime.Now, "Central Cofe", 0);
            dal.addCoupon(c);
            dal.approveCoupon("FreeCofe");
            Assert.IsTrue(dal.getCouponByName("FreeCofe").approved);
            dal.removeCoupon("FreeCofe");
        }

        [Test] //9
        public void orderCoupon()
        {
            Store s = new Store("CentralCofe", "bob", "Hatikva", "Haifa", "a Cofe shop", Category.Food, "Central Cofe", "121212", "049999222", "cofe@mail.com");
            dal.addStore(s);
            Coupon c = new Coupon("FreeCofe", "a free cofe just for over 25 years old", 8, 0, 5, 10, Category.Food, DateTime.Now, "Central Cofe", 0);
            dal.addCoupon(c);
            dal.orderCoupon(dal.getCouponByName("FreeCofe"), 1, "noam");
            Assert.IsTrue(dal.getAllCouponOrdersFromStore("Central Cofe").Count > 0);
            dal.removeCoupon("FreeCofe");
            dal.removeStore(s.username);
        }

        [Test]  //10
        public void useCoupon()
        {
            Order c = dal.getOrderedCouponsByUsername("noam").ElementAt(0);
            dal.fulfillCouponAndApprove(c);
            Assert.IsTrue(dal.getOrderedCouponsByUsername("noam").ElementAt(0).isUsed);
        }

        [Test]  //11
        public void editCoupon()
        {
            Coupon c1 = new Coupon("FreeCofe", "a free cofe just for over 15 years old", 8, 0, 5, 10, Category.Food, DateTime.Now, "Central Cofe", 0);
            Coupon c = new Coupon("NotSoFreeCofe" , "a free cofe just for over 25 years old" , 8 ,5 , 5 , 10 , Category.Food , DateTime.Now , "Central Cofe" , 0);
            dal.addCoupon(c);
            dal.addCoupon(c1);
            dal.editCoupon("FreeCofe", c);
            Assert.IsTrue(dal.getCouponByName("FreeCofe").description.Equals("a free cofe just for over 25 years old"));
            dal.removeCoupon("FreeCofe");
            dal.removeCoupon("NotSoFreeCofe");
        }

        [Test]  //12
        public void removeCoupon()
        {
            Coupon c = new Coupon("NotSoFreeCofe", "a free cofe just for over 25 years old", 8, 5, 5, 10, Category.Food, DateTime.Now, "Central Cofe", 0);
            dal.addCoupon(c);
            Assert.IsTrue(dal.isCouponAlreadyExists("NotSoFreeCofe"));
            dal.removeCoupon("NotSoFreeCofe");
            Assert.IsFalse(dal.isCouponAlreadyExists("NotSoFreeCofe"));
        }

        [Test]  //13
        public void editStore()
        {
            dal.removeStore("CentralCofe");
            Store s = new Store("CentralCofe" , "Roslana" , "Hatikva" , "Haifa" , "a Cofe shop" , Category.Food , "Central Cofe" , "121212" , "049999222", "cofe@mail.com");
            dal.addStore(s);
            dal.editStoreDetails("CentralCofe", s);
            Assert.IsFalse(dal.getStoreByUsername("CentralCofe").ownerName.Equals("David"));
            dal.removeStore("CentralCofe");
        }

        [Test]  //14
        public void addOwner()
        {
            dal.removeStore("CentralCofe");
            Store s = new Store("CentralCofe", "Roselana", "Hatikva", "Haifa", "a Cofe shop", Category.Food, "Central Cofe", "121212", "049999222", "cofe@mail.com");
            dal.addStore(s);
            dal.addOwnerToStore(s,"Kokilida");
            Assert.IsTrue(dal.getStoreByUsername("CentralCofe").ownerName.Equals("Kokilida"));
            dal.removeStore(s.username);
        }

        [Test]  //15
        public void removeStore()
        {
            dal.removeStore("CentralCofe");
            Store s = new Store("CentralCofe", "Roselana", "Hatikva", "Haifa", "a Cofe shop", Category.Food, "Central Cofe", "121212", "049999222", "cofe@mail.com");
            dal.addStore(s);
            dal.removeStore("CentralCofe");
            Assert.IsNull(dal.getStoreByUsername("CentralCofe"));
        }
        
    }
}
