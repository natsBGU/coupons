﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;
using BL;
using NUnit.Framework;

namespace CouponTest
{
    [TestFixture]
    class TestsManager
    {
        IDAL dal;
        ManagerController con;
        Coupon c, c1, c2;
        SignedUser u;
        Store s, s1;
        Manager m;
        public TestsManager()
        {
            dal = new DAL.DAL();
            con = new ManagerControllerImp(dal);

            u = new SignedUser("RandomUser", "Haifa", "121212", "089974312", "maxstar112233@gmail.com");
            s = new Store("RandomStore", "bob", "Not", "Haifa", "store for testing", Category.Other, "Random Store", "12", "048899898", "store@gmail.com");
            s1 = new Store("RandomStore1", "mob", "Not", "Tel aviv", "store for testing2", Category.Food, "Random Store1", "12", "048899828", "store1@gmail.com");
            c = new Coupon("RandomCoupon", "testing cpupon", 10, 5, 0, 0, Category.Other, DateTime.Today, "Random Store", 0);
            c1 = new Coupon("RandomCoupon1", "testing cpupon", 10, 5, 0, 0, Category.Other, DateTime.Today, "Random Store", 0);
            c2 = new Coupon("RandomCoupon2", "testing cpupon", 10, 5, 0, 0, Category.Food, DateTime.Today, "Random Store1", 0);
            m = new Manager("man" ,"1","666" , "iAmTheMan213123143412342@gmail.com");

            dal.removeSignedUserByUsername(u.username);
            dal.removeStore(s.username);
            dal.removeStore(s1.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
            dal.removeCoupon(c2.name);
        }

        [Test] //1
        public void exsitUserName()
        {
            List<Category> l = new List<Category>();
            l.Add(Category.Food);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addUser(u.username, u.password, u.phone, u.mail, l);
            Assert.IsTrue(con.usernameExist(u.username));
            con.removeSignedUser(u.username);

        }


        [Test] //2
        public void addAndRemoveManager()
        {
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            Assert.IsTrue(con.usernameExist(m.username));
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }

        [Test] //3
        public void managerClerance()
        {
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            Assert.IsTrue(con.usernameExist(m.username));
            Assert.AreEqual(con.GetManagerByUsername(m.username).clearence , 0);
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }

        [Test] //5
        public void addAndRemoveCoupon()
        {
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            con.addCoupon(c);
            Assert.IsTrue(con.couponExist(c.name));
            con.removeCoupon(c.name);
            Assert.IsFalse(con.couponExist(c.name));
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }

        [Test] //5
        public void addAndRemoveStore()
        {
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            con.addStore(s);
            Assert.IsTrue(con.usernameExist(s.username));
            con.removeStore(s.username);
            Assert.IsFalse(con.usernameExist(s.username));
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }


        [Test] //6
        public void editStore()
        {

            Store s2 = new Store("RandomStore", "bob", "Not", "Haifa", "newwwwwww", Category.Other, "Random Store", "12", "048899898", "store@gmail.com");
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            con.addStore(s);
            Assert.IsTrue(con.usernameExist(s.username));
            con.editStore(s.username, s2);
            Assert.AreEqual(con.GetStoreByUsername(s.username).description, "newwwwwww");
            con.removeStore(s.username);
            Assert.IsFalse(con.usernameExist(s.username));
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }

        [Test] //7
        public void editCoupon()
        {
            Coupon tmp = new Coupon("RandomCoupon", "say what?", 10, 5, 0, 0, Category.Other, DateTime.Today, "Random Store", 0);
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            con.addCoupon(c);
            Assert.IsTrue(con.couponExist(c.name));
            con.editCoupon(c.name, tmp);
            Assert.AreEqual(con.getCouponByName(c.name).description, "say what?");
            con.removeCoupon(c.name);
            Assert.IsFalse(con.couponExist(c.name));
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }


        [Test] //8
        public void editManager()
        {
            Manager m1 = new Manager("man" ,"1","05053344556" , "iAmTheMan213123143412342@gmail.com");
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            Assert.IsTrue(con.usernameExist(m.username));
            con.editManager(m.username, m1);
            Assert.AreEqual(con.GetManagerByUsername(m.username).phone, "05053344556");
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
        }

        [Test] //9
        public void numOfMan()
        {
            Manager m1 = new Manager("man1", "1", "05053344556", "iAmTheMan21311123143412342@gmail.com");
            con.removeManager(m.username);
            Assert.IsFalse(con.usernameExist(m.username));
            con.addManager(m);
            con.addManager(m1);
            Assert.AreEqual(con.getAllManagers().Count , 4);
            con.removeManager(m.username);
            con.removeManager(m1.username);
        }
    }
}
