﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;
using BL;
using NUnit.Framework;

namespace CouponTest
{
    [TestFixture]
    class TestsStore
    {
        IDAL dal;
        StoreController con;
        Coupon c, c1 , c2;
        SignedUser u;
        Store s , s1;
        public TestsStore()
        {
            dal = new DAL.DAL();
            con = new StoreControllerImp(dal);
            u = new SignedUser("RandomUser", "Haifa", "121212", "089974312", "maxstar112233@gmail.com");
            s = new Store("RandomStore", "bob", "Not", "Haifa", "store for testing", Category.Other, "Random Store", "12", "048899898", "store@gmail.com");
            s1 = new Store("RandomStore1", "mob", "Not", "Tel aviv", "store for testing2", Category.Food, "Random Store1", "12", "048899828", "store1@gmail.com");
            c = new Coupon("RandomCoupon", "testing cpupon", 10, 5, 0, 0, Category.Other, DateTime.Today, "Random Store", 0);
            c1 = new Coupon("RandomCoupon1", "testing cpupon", 10, 5, 0, 0, Category.Other, DateTime.Today, "Random Store", 0);
            c2 = new Coupon("RandomCoupon2", "testing cpupon", 10, 5, 0, 0, Category.Food, DateTime.Today, "Random Store1", 0);

            dal.removeSignedUserByUsername(u.username);
            dal.removeStore(s.username);
            dal.removeStore(s1.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
            dal.removeCoupon(c2.name);

        }

        [Test] // 1
        public void getStore()
        {
            dal.addStore(s);
            dal.addStore(s1);
            Assert.AreNotEqual(con.GetStoreByUsername(s.username), con.GetStoreByUsername(s1.username));
            Assert.AreEqual(con.GetStoreByUsername(s.username).description, "store for testing");
            dal.removeStore(s.storeName);
            dal.removeStore(s1.storeName);
        }


        [Test] // 2
        public void notApproved()
        {
            dal.addStore(s);
            dal.addStore(s1);
            con.addCoupon(c);
            con.addCoupon(c1);
            con.addCoupon(c2);

            Assert.AreEqual(con.getAllUnapprovedCoupons().Count, 5);
            dal.approveCoupon(c.name);
            Assert.AreEqual(con.getAllUnapprovedCoupons().Count, 4);
            dal.approveCoupon(c1.name);
            Assert.AreEqual(con.getAllUnapprovedCoupons().Count, 3);
            dal.approveCoupon(c2.name);
            Assert.AreEqual(con.getAllUnapprovedCoupons().Count, 2);

            dal.removeStore(s.storeName);
            dal.removeStore(s1.storeName);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
            dal.removeCoupon(c2.name);
        }


        [Test] // 3
        public void order()
        {
            List<Category> l = new List<Category>();
            l.Add(Category.Food);
            l.Add(Category.Sport);
            l.Add(Category.Other);
            dal.addStore(s);
            dal.addUser(u.username, u.password, u.phone, u.mail, l);
            con.addCoupon(c);
            con.addCoupon(c1);
            int count = con.myStoreOrders(s.storeName).Count;
            dal.orderCoupon(c, 3, "RandomUser");
            Assert.AreEqual(con.myStoreOrders(s.storeName).Count , count + 1);
            dal.orderCoupon(c1, 3, "RandomUser");
            Assert.AreEqual(con.myStoreOrders(s.storeName).Count, count + 2);
            dal.removeSignedUserByUsername(u.username);
            dal.removeStore(s.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
        }

        [Test] // 4
        public void approved()
        {
            dal.addStore(s);
            dal.addStore(s1);
            con.addCoupon(c);
            con.addCoupon(c1);
            con.addCoupon(c2);

            Assert.AreEqual(con.getAllApprovedCoupons().Count, 5);
            dal.approveCoupon(c.name);
            Assert.AreEqual(con.getAllApprovedCoupons().Count, 6);
            dal.approveCoupon(c1.name);
            Assert.AreEqual(con.getAllApprovedCoupons().Count, 7);
            dal.approveCoupon(c2.name);
            Assert.AreEqual(con.getAllApprovedCoupons().Count, 8);

            dal.removeStore(s.storeName);
            dal.removeStore(s1.storeName);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
            dal.removeCoupon(c2.name);
        }

        [Test] // 5
        public void useMyCoupon()
        {
            dal.addStore(s);
            dal.addStore(s1);
            con.addCoupon(c);
            con.addCoupon(c1);
            con.addCoupon(c2);

            int count = con.myStoreOrders(s.storeName).Count;
            dal.orderCoupon(c, 3, "RandomUser");
            dal.orderCoupon(c1, 3, "RandomUser");
            List<Order> o = con.myStoreOrders(s.storeName);
            Assert.AreEqual(o.Count , count + 2);
 
            dal.removeStore(s.storeName);
            dal.removeStore(s1.storeName);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
            dal.removeCoupon(c2.name);
        }


        [Test] // 6
        public void myCoupons()
        {
            dal.addStore(s);
            con.addCoupon(c);
            con.addCoupon(c1);

            Assert.AreEqual(con.getStoreCoupons(s.storeName).Count, 2);

            dal.removeStore(s.username);
            dal.removeCoupon(c.name);
            dal.removeCoupon(c1.name);
        }

    }
}
