CREATE DATABASE NATSCoupons;
GO
USE NATSCoupons;
GO
CREATE TABLE Users(
username varchar(50) primary key,
password varchar(50)  not null,
phone varchar(50) not null,
mail varchar(100) not null,
clearence int not null,
);

CREATE TABLE Category(
name varchar(50) primary key
);

CREATE TABLE Store(
username varchar(50) foreign key REFERENCES Users(username),
ownerName varchar(50) not null,
address varchar(100) not null,
city varchar(50) not null,
description varchar(100),
category varchar(50) foreign key REFERENCES Category(name),
name varchar(100) not null,
CONSTRAINT pkstore PRIMARY KEY (username)
);

CREATE TABLE Social(
username varchar(50) foreign key REFERENCES Users(username),
networkname varchar(50) not null,
CONSTRAINT pksocial PRIMARY KEY (username, networkname)
);

CREATE TABLE SignedUser(
username varchar(50) foreign key REFERENCES Users(username),
lastLocation varchar(100),
CONSTRAINT pksigned PRIMARY KEY (username)
);

CREATE TABLE Manager(
username varchar(50) foreign key REFERENCES Users(username),
CONSTRAINT pkmanager PRIMARY KEY (username)
);

CREATE TABLE Coupon(
name varchar(50) primary key,
description varchar(100),
price integer not null,
priceAfterDiscount integer not null,
rate integer default 0,
numOfRaters int default 0,
category varchar(50) foreign key REFERENCES Category(name),
expiration date not null,
storeName varchar(50) unique not null,
approved int default 0,
);

CREATE TABLE Orders(
serialKey varchar(50) primary key,
orderDate date not null,
quantity int default 1,
username varchar(50) foreign key REFERENCES Users(username),
couponName varchar(50) foreign key REFERENCES Coupon(name),
isUsed varchar(50) not null,
);

CREATE TABLE SignedUserCatogories(
username varchar(50) foreign key REFERENCES Users(username),
category varchar(50) foreign key REFERENCES Category(name),
CONSTRAINT pkusercat PRIMARY KEY (username,category)
);

