﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace DAL
{
    public interface IDAL
    {
        //************General queries************
        bool isUsernameAlreadyExists(string username);

        bool isCouponAlreadyExists(string couponName);

        int userLogin(string username, string password);

        void addUser(string username, string password, string phone, string mail, List<Category> categories);

        List<Coupon> getAllApprovedCoupons();

        List<Coupon> getAllNotApprovedCoupons();

        List<Coupon> getAllCoupons();

        //************SignedUser queries (clearence level 2)************
        List<Order> getOrderedCouponsByUsername(string username);

        void rateCoupon(Coupon c, int rate);

        SignedUser getSignedUserByUsername(string username);

        void orderCoupon(Coupon c, int quantity, string username);

        int removeSignedUserByUsername(string username);

        List<Coupon> getCouponsByStoreNameCityAndCategories(string store, string city, List<Category> cs);

        List<Store> getStoresByStoreNameCityAndCategories(string store, string city, List<Category> cs);

        string signUserEmail(string username);


        //************Store queries (clearence level 1)************
        void addCoupon(Coupon c);

        List<Order> getAllCouponOrdersFromStore(string storeName);

        Store getStoreDetails(string storeName);

        Store getStoreByUsername(string username);

        void editStoreDetails(string username, Store newStore);

        Order getOrderBySerialKey(string serial);

        bool fulfillCouponAndApprove(Order o);

        List<Coupon> getCouponsByStoreName(string storeName);

        //************Manager queries (clearence level 0)************
        void removeCoupon(string name);

        void approveCoupon(string name);

        Manager getManagerByUsername(string username);

        void addStore(Store s);

        void removeStore(string username);

        void addOwnerToStore(Store s, string owner);

        bool isStoreHasOwner(Store s);

        void addManager(string username, string password, string phone, string mail);

        void editCoupon(string couponName, Coupon newCoupon);

        void editManager(string username, Manager newManager);

        Coupon getCouponByName(string name);

        void removeManagerByUsername(string username);

        List<Manager> getAllManagers();
    }
}
