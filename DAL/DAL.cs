﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BL_Backend;
using System.IO;

namespace DAL
{
    public class DAL : IDAL
    {
        private SqlConnection myConnection;

        public DAL()
        {
            SQLConnect();
        }

        public void SQLConnect()
        {
            myConnection = new SqlConnection(Properties.Settings.Default.ConnString);
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                myConnection.Close();
            }
        }

        public void ExecuteNonQuery(string query)
        {
            try
            {
                myConnection.Open();
                SqlCommand command = new SqlCommand(query, myConnection);
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(query);
                Console.WriteLine("\n");
                Console.WriteLine(e.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                myConnection.Close();
            }
        }

        public List<Category> ExecuteQuerySelectCategory(string sql)
        {
            List<Category> categories = new List<Category>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    categories.Add((Category)(Enum.Parse(typeof(Category),rdr.GetString(0))));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return categories;
        }

        public List<Coupon> ExecuteQuerySelectCoupons(string sql)
        {
            List<Coupon> coupons = new List<Coupon>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    coupons.Add(new Coupon(rdr.GetString(0), rdr.GetString(1), rdr.GetInt32(2), rdr.GetInt32(3), rdr.GetInt32(4), rdr.GetInt32(5), ((Category)Enum.Parse(typeof(Category), rdr.GetString(6))), rdr.GetDateTime(7), rdr.GetString(8), rdr.GetInt32(9)));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return coupons;
        }

        public List<Store> ExecuteQuerySelectStores(string sql)
        {
            List<Store> stores = new List<Store>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    stores.Add(new Store(rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4), ((Category)Enum.Parse(typeof(Category), rdr.GetString(5))), rdr.GetString(6), rdr.GetString(7), rdr.GetString(8), rdr.GetString(9)));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return stores;
        }

        public List<SignedUser> ExecuteQuerySelectSignedUser(string sql)
        {
            List<SignedUser> signedUsers = new List<SignedUser>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    signedUsers.Add(new SignedUser(rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetString(4)));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return signedUsers;
        }


        public List<Manager> ExecuteQuerySelectManager(string sql)
        {
            List<Manager> manager = new List<Manager>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    manager.Add(new Manager(rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3)));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return manager;
        }

        public List<Users> ExecuteQuerySelectUsers(string sql)
        {
            List<Users> users = new List<Users>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    users.Add(new Users(rdr.GetString(0), rdr.GetString(1), rdr.GetString(2), rdr.GetString(3), rdr.GetInt32(4)));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return users;
        }

        public List<Order> ExecuteQuerySelectOrders(string sql)
        {
            List<Order> orders = new List<Order>();
            SqlDataReader rdr = null;
            try
            {
                myConnection.Open();

                SqlCommand cmd = new SqlCommand(sql, myConnection);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    bool isUsed = (rdr.GetString(5) == "0") ? false : true;
                    orders.Add(new Order(rdr.GetString(0), rdr.GetDateTime(1), rdr.GetInt32(2), rdr.GetString(3), rdr.GetString(4), isUsed));
                }
            }
            catch (SqlException se)
            {
                Console.WriteLine("Error while executing query: ");
                Console.WriteLine(sql);
                Console.WriteLine("\n");
                Console.WriteLine(se.Message);
                Console.WriteLine("\n");
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }

                if (myConnection != null)
                {
                    myConnection.Close();
                }
            }
            return orders;
        }

        //************General queries************

        public bool isUsernameAlreadyExists(string username)
        {
            string query =
                "SELECT * " +
                "FROM Users "+
                "WHERE username = '"+username+"'";
            List<Users> su = ExecuteQuerySelectUsers(query);
            if (su.Count == 0)
                return false;
            return true;
        }

        public string signUserEmail(string username)
        {
            string qurey = 
                "SELECT *" +
                "FROM Users " +
                "WHERE username = '" + username + "'";
            List<Users> su = ExecuteQuerySelectUsers(qurey);
            if (su.Count() == 0)
                return "";
            return su.First<Users>().mail;
        }

        public bool isCouponAlreadyExists(string couponName)
        {
            string query =
                "SELECT * " +
                "FROM Coupon " +
                "WHERE name = '" + couponName + "'";
            List<Coupon> cs = ExecuteQuerySelectCoupons(query);
            if (cs.Count() == 0)
                return false;
            return true;
        }

        public int userLogin(string username, string password)
        {
            string query =
                "SELECT * " +
                "FROM Users " +
                "WHERE username = '" + username + "' AND password = '" + password + "'";
            List<Users> su = ExecuteQuerySelectUsers(query);
            if (su.Count == 0)
                return -1;
            return su.First<Users>().clearence; 
        }

        public void addUser(string username, string password, string phone, string mail, List<Category> categories)
        {
            string location = GPS.getLocation();
            string query =
                "INSERT INTO Users(username, password, phone, mail, clearence) " +
                "VALUES('" + username + "','" + password + "','" + phone + "','" + mail + "',2)";
            ExecuteNonQuery(query);
            query = "INSERT INTO SignedUser(username, lastLocation) VALUES('" + username + "','"+location+"')";
            ExecuteNonQuery(query);
            foreach (Category c in categories)
            {
                query = "INSERT INTO SignedUserCatogories(username, category) VALUES('" + username + "','"+ c.ToString() +"')";
                ExecuteNonQuery(query);
            }
        }

        public List<Coupon> getAllApprovedCoupons()
        {
            string query =
                "SELECT * " +
                "FROM Coupon " +
                "WHERE approved = 1";
            List<Coupon> c = ExecuteQuerySelectCoupons(query);
            return c;
        }

        public List<Coupon> getAllNotApprovedCoupons()
        {
            string query =
                "SELECT * " +
                "FROM Coupon " +
                "WHERE approved = 0";
            List<Coupon> c = ExecuteQuerySelectCoupons(query);
            return c;
        }

        public List<Coupon> getAllCoupons()
        {
            string query =
                "SELECT * " +
                "FROM Coupon";
            List<Coupon> c = ExecuteQuerySelectCoupons(query);
            return c;
        }

        //************SignedUser queries (clearence level 2)************

        public List<Order> getOrderedCouponsByUsername(string username)
        {
            string query =
                "SELECT Orders.* " +
                "FROM Orders " +
                "WHERE Orders.username = '" + username + "'";
            List<Order> os = ExecuteQuerySelectOrders(query);
            return os;
        }

        public void rateCoupon(Coupon c, int rate)
        {
            int newRate = (c.rate * c.numOfRaters + rate) / (c.numOfRaters + 1);
            string query =
               "UPDATE Coupon " +
               "SET rate=" + newRate + ",numOfRaters="+(c.numOfRaters+1) +" "+
               "WHERE Coupon.name = '" + c.name + "'";
            ExecuteNonQuery(query);
        }

        public void orderCoupon(Coupon c, int quantity, string username)
        {
            int serialkey;
            Order o;
            Random rand = new Random();
            do
            {
                serialkey = rand.Next(100000000, 999999999);
                o = getOrderBySerialKey("" + serialkey);
            } while (o != null);

            string query =
                "INSERT INTO Orders(serialKey,orderDate,quantity,username,couponName,isUsed) " +
                "VALUES('" + serialkey + "','" + DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss") + "'," + quantity + ",'" + username + "','" + c.name + "','0')";
            ExecuteNonQuery(query);
            string email = signUserEmail(username);
            string subject = "Receipt for your coupon order";
            StringBuilder body = new StringBuilder();
            body.Append("Serial Key: ");
            body.Append(serialkey);
            body.Append("\nDate: ");
            body.Append(DateTime.Now.ToString());
            body.Append("\nQuantity: ");
            body.Append(quantity);
            body.Append("\nCoupon Name: ");
            body.Append(c.name);
            body.Append("\n\nThank you for your purchase.\nHope to see you again!");
            MailHandler.sendMail(email, subject, body.ToString());
        }

        public List<Coupon> getCouponsByStoreNameCityAndCategories(string store, string city, List<Category> cs)
        {
            string query =
                 "SELECT Coupon.* " +
                 "FROM Coupon JOIN Store " +
                 "ON Coupon.storeName=Store.name "+
                 "WHERE ";
            if (store != null)
            {
                query += "Coupon.storeName='" + store + "' AND ";
            }
            if (city != null)
            {
                query += "Store.city='" + city + "' AND ";
            }
            if (cs.Count > 0)
            {
                query += "(";
                foreach (Category c in cs)
                {
                    query += "Coupon.category='" + c.ToString() + "' OR ";
                }
                query = query.Substring(0, query.Length - 4);
                query += ") AND ";
            }
            query = query.Substring(0, query.Length - 5);
            Console.WriteLine(query);
            List<Coupon> coupons = ExecuteQuerySelectCoupons(query);
            return coupons;
        }

        public List<Store> getStoresByStoreNameCityAndCategories(string store, string city, List<Category> cs)
        {
            string query =
                "SELECT Store.*, Users.password, Users.phone, Users.mail " +
                "FROM Store JOIN Users " +
                "ON Store.username = Users.username " +
                "WHERE ";
            if (store != null)
            {
                query += "Store.name='" + store + "' AND ";
            }
            if (city != null)
            {
                query += "Store.city='" + city + "' AND ";
            }
            if (cs.Count > 0)
            {
                query += "(";
                foreach (Category c in cs)
                {
                    query += "Store.category='" + c.ToString() + "' OR ";
                }
                query = query.Substring(0, query.Length - 4);
                query += ") AND ";
            }
            query = query.Substring(0, query.Length - 5);
            List<Store> stores = ExecuteQuerySelectStores(query);
            return stores;  
        }

        //************Store queries (clearence level 1)************

        public void addCoupon(Coupon c)
        {
            int approved = (c.approved == true) ? 1 : 0;
            DateTime date = c.expiration.AddMonths(1);
            string query = 
                "INSERT INTO Coupon(name,description,price,priceAfterDiscount,rate,numOfRaters,category,expiration,storeName,approved) "+
                "VALUES('" + c.name + "','" + c.description + "'," + c.price + "," + c.priceAfterDiscount + ",0,0,'" + c.category.ToString() + "','" + date.ToString("yyyy-MM-dd HH:mm:ss") + "','" + c.storeName + "',"+approved+")";
            ExecuteNonQuery(query);
        }

        public List<Order> getAllCouponOrdersFromStore(string storeName)
        {
            string query =
                "SELECT Orders.* " +
                "FROM Orders JOIN Coupon " +
                "ON Orders.couponName = Coupon.name " +
                "WHERE Coupon.storeName = '" + storeName + "'";
            List<Order> o = ExecuteQuerySelectOrders(query);
            return o;
        }

        public Store getStoreDetails(string storeName)
        {
           string query =
               "SELECT * " +
               "FROM Store " +
               "WHERE Store.name = '" + storeName + "'";
           List<Store> ls = ExecuteQuerySelectStores(query);
           if (ls.Count() == 0)
               return null;
           return ls.First<Store>();
        }

        public void editStoreDetails(string username, Store newStore)
        {
            string query =
               "UPDATE Users " +
               "SET phone='" + newStore.phone + "',mail='" + newStore.mail + "' " +
               "WHERE Users.username = '" + username + "'";
            ExecuteNonQuery(query);
            query =
               "UPDATE Store " +
               "SET address='" + newStore.address + "',city='" + newStore.city + "',"+
               "description='" + newStore.description + "',category='" + newStore.category.ToString()+ "' " +
               "WHERE Store.username = '" + username + "'";
            ExecuteNonQuery(query);
        }

        public Order getOrderBySerialKey(string serial)
        {
            string query =
                "SELECT * " +
                "FROM Orders " +
                "WHERE Orders.serialKey = '" + serial + "'";
            List<Order> o = ExecuteQuerySelectOrders(query);
            if (o.Count() == 0)
                return null;
            return o.First<Order>();
        }

        public bool fulfillCouponAndApprove(Order o)
        {
            Order order = getOrderBySerialKey(o.serial);
            if (order == null || order.isUsed == true)
                return false;
            changeCouponStateToUsed(o);
            return true;
        }

        private void changeCouponStateToUsed(Order o)
        {
            string query =
                "UPDATE Orders " +
                "SET isUsed = '1' " +
                "WHERE serialKey = '" + o.serial + "' ";
            ExecuteNonQuery(query);
        }

        public List<Coupon> getCouponsByStoreName(string storeName)
        {
            string query =
                "SELECT Coupon.* " +
                "FROM Coupon " +
                "WHERE Coupon.storeName = '" + storeName + "'";
            List<Coupon> c = ExecuteQuerySelectCoupons(query);
            return c;
        }

        //************Manager queries (clearence level 0)************

        public void removeCoupon(string name)
        {
            string query =
                "DELETE FROM Coupon " +
                "WHERE name = '" + name + "' ";
            ExecuteNonQuery(query);
        }

        public void approveCoupon(string name)
        {
            string query =
                "UPDATE Coupon " +
                "SET approved = 1 "+
                "WHERE name = '" + name + "' ";
            ExecuteNonQuery(query);
        }

        public void addStore(Store s)
        {
            string query =
                "INSERT INTO Users(username,password,phone,mail,clearence) " +
                "VALUES('" + s.username + "','" + s.password + "','" + s.phone + "','" + s.mail + "',1)";
            ExecuteNonQuery(query);
            query =
                "INSERT INTO Store(username,ownerName,address,city,description,category,name) " +
                "VALUES('" + s.username + "','1','" + s.address + "','" + s.city + "','" + s.description + "','" + s.category.ToString() + "','"+s.storeName+"')";
            ExecuteNonQuery(query);
        }

        public void removeStore(string username)
        {
            string query =
                "DELETE FROM Store " +
                "WHERE username = '" + username + "' ";
            ExecuteNonQuery(query);
            query =
                "DELETE FROM Users " +
                "WHERE username = '" + username + "' ";
            ExecuteNonQuery(query);
        }

        public void addOwnerToStore(Store s, string owner)
        {
            if (isStoreHasOwner(s))
                return;
            string query =
                "UPDATE Store " +
                "SET ownerName = '" + owner + "' " +
                "WHERE username = '" + s.username + "' ";
            ExecuteNonQuery(query);
        }

        public bool isStoreHasOwner(Store s)
        {
            string query =
                "SELECT * " +
                "FROM Store " +
                "WHERE name = '" + s.username + "' ";
            List<Store> ls = ExecuteQuerySelectStores(query);
            if (ls.Count() == 0)
                return false;
            Store tmp = ls.First<Store>();
            return (tmp.ownerName != null);
        }

        public SignedUser getSignedUserByUsername(string username)
        {
            string query =
                "SELECT SignedUser.*, Users.password, Users.phone, Users.mail " +
                "FROM SignedUser JOIN Users " +
                "ON SignedUser.username = Users.username " +
                "WHERE SignedUser.username = '" + username + "'";
            List<SignedUser> o = ExecuteQuerySelectSignedUser(query);
            if (o.Count() == 0)
                return null;
            SignedUser su = o.First<SignedUser>();
            query =
                "SELECT SignedUserCatogories.category " +
                "FROM SignedUserCatogories " +
                "WHERE username = '" + username + "'";
            List<Category> c = ExecuteQuerySelectCategory(query);
            foreach (Category cat in c)
                su.addCategory(cat);            
            return su;
        }

        public Store getStoreByUsername(string username)
        {
            /* string username, string address, string city, string description, Category category, string storeName, string password, string phone, string mail*/
            string query =
                "SELECT Store.*, Users.password, Users.phone, Users.mail  " +
                "FROM Store JOIN Users " +
                "ON Store.username = Users.username " +
                "WHERE Store.username = '" + username + "'";
            List<Store> o = ExecuteQuerySelectStores(query);
            if (o.Count() == 0)
                return null;
            return o.First<Store>();
        }

        public Manager getManagerByUsername(string username)
        {
            string query =
                "SELECT Users.* " +
                "FROM Manager JOIN Users " +
                "ON Manager.username = Users.username "+
                "WHERE Manager.username = '" + username + "'";
            List<Manager> o = ExecuteQuerySelectManager(query);
            if (o.Count() == 0)
                return null;
            return o.First<Manager>();
        }

        public int removeSignedUserByUsername(string username)
        {
            string query =
                "DELETE FROM SignedUserCategories " +
                "WHERE username = '" + username + "'";
            ExecuteNonQuery(query);
            query =
                "DELETE FROM SignedUser " +
                "WHERE username = '" + username + "'";
            ExecuteNonQuery(query);
            query =
                "DELETE FROM Users " +
                "WHERE username = '" + username + "'";
            ExecuteNonQuery(query);
            return (isUsernameAlreadyExists(username))? -1 : 1;
        }

        public void addManager(string username, string password, string phone, string mail)
        {
            string query =
                "INSERT INTO Users(username, password, phone, mail, clearence) " +
                "VALUES('" + username + "','" + password + "','" + phone + "','" + mail + "',0)";
            ExecuteNonQuery(query);
            query = "INSERT INTO Manager(username) VALUES('" + username +"')";
            ExecuteNonQuery(query);
        }

        public void editCoupon(string couponName, Coupon newCoupon)
        {
            string query =
               "UPDATE Coupon " +
               "SET description='" + newCoupon.description + "',price=" + newCoupon.price + "," +
               "priceAfterDiscount=" + newCoupon.priceAfterDiscount + ",category='" + newCoupon.category.ToString() + "'," +
               "storeName='" + newCoupon.storeName + "',approved=" + ((newCoupon.approved) ? 1 : 0) + " " +
               "WHERE Coupon.name = '" + couponName + "'";
            ExecuteNonQuery(query);
        }

        public void editManager(string username, Manager newManager)
        {
            string query =
               "UPDATE Users " +
               "SET password='" + newManager.password + "',phone='" + newManager.phone + "', " +
               "mail='" + newManager.mail + "' " +
               "WHERE Users.username = '" + username + "'";
            ExecuteNonQuery(query);
        }

        public Coupon getCouponByName(string name)
        {
            string query =
                "SELECT Coupon.* " +
                "FROM Coupon " +
                "WHERE Coupon.name = '" + name + "'";
            List<Coupon> c = ExecuteQuerySelectCoupons(query);
            if (c.Count == 0)
                return null;
            return c.First<Coupon>();
        }

        public void removeManagerByUsername(string username)
        {
            string query =
                "DELETE FROM Manager " +
                "WHERE username = '" + username + "'";
            ExecuteNonQuery(query);
            query =
                "DELETE FROM Users " +
                "WHERE username = '" + username + "'";
            ExecuteNonQuery(query);
        }

        public List<Manager> getAllManagers()
        {
            string query =
                "SELECT Users.* " +
                "FROM Users JOIN Manager " +
                "ON Users.username = Manager.username";
            List<Manager> ms = ExecuteQuerySelectManager(query);
            if (ms.Count == 0)
                return null;
            return ms;
        }
    }
}
