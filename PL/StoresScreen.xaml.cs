﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace PL
{
	/// <summary>
	/// Interaction logic for StoreMenu.xaml
	/// </summary>
	public partial class StoresScreen : UserControl
	{
        private MainWindow main;
        private UserMenu user;
        private Store[] Stores;
        private int currentIndex;
        private int size;
        private string username;

        public StoresScreen(MainWindow main, UserMenu user, List<Store> Stores, string username)
		{
			this.InitializeComponent();
            this.main = main;
            this.user = user;
            this.Stores = Stores.ToArray();
            this.currentIndex = 0;
            this.size = Stores.Count;
            this.username = username;
            if (size > 0)
            {
                displayStore(Stores[0]);
                updateIsEnable();
            }
		}
        private void prevButtonClick(object sender, RoutedEventArgs e)
        {
            currentIndex--;
            displayStore(Stores[currentIndex]);
            updateIsEnable();
        }

        private void nextButtonClick(object sender, RoutedEventArgs e)
        {
            currentIndex++;
            displayStore(Stores[currentIndex]);
            updateIsEnable();
        }

        private void backButtonClick(object sender, RoutedEventArgs e)
        {
            this.main.Content = user;
        }

        private void updateIsEnable()
        {
            this.prevButton.IsEnabled = true;
            this.nextButton.IsEnabled = true;
            if (currentIndex == 0)
            {
                this.prevButton.IsEnabled = false;
            }
            if (currentIndex == size-1)
            {
                this.nextButton.IsEnabled = false;
            }
        }

        private void displayStore(Store s)
        {
            storeName.Text = s.storeName;
            address.Text = s.address + ", " + s.city;
            description.Text = s.description;
            category.Text = s.category.ToString();
            phone.Text = s.phone;
            mail.Text = s.mail;
        }

	}
}