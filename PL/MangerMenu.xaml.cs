﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace PL
{
	/// <summary>
	/// Interaction logic for MangerMenu.xaml
	/// </summary>
	public partial class MangerMenu : UserControl
	{
        private MainWindow main;
        private LoginScreen ls;
        private ManagerController controller;

        public MangerMenu(MainWindow main, LoginScreen ls, ManagerController controller)
		{
			InitializeComponent();
            this.main = main;
            this.ls = ls;
            this.controller = controller;
            var categories = Enum.GetValues(typeof(BL_Backend.Category));
            foreach (Category c in categories)
            {
                asCategory.Items.Add(c.ToString());
                acCategory.Items.Add(c.ToString());
                ecCategory.Items.Add(c.ToString());
            }
            acApproved.Items.Add("true");
            acApproved.Items.Add("false");
            ecApproved.Items.Add("true");
            ecApproved.Items.Add("false");

            hideAllMenu();
            hideAllCouponMenu();
            Add_Store.Visibility = System.Windows.Visibility.Visible;
            Add_Coupon.Visibility = System.Windows.Visibility.Visible;
		}

        private void AddStoreMenuButton(object sender, RoutedEventArgs e)
        {
            hideAllMenu();
            Add_Store.Visibility = System.Windows.Visibility.Visible;
        }

        private void EditCouponMenuButton(object sender, RoutedEventArgs e)
        {
            hideAllMenu();
            Edit_Coupon.Visibility = System.Windows.Visibility.Visible;
        }

        private void EditManagerMenuButton(object sender, RoutedEventArgs e)
        {
            hideAllMenu();
            Edit_Manager.Visibility = System.Windows.Visibility.Visible;
        }

        private void LogoutMenuButton(object sender, RoutedEventArgs e)
        {
            main.Content = ls;
        }

        private void AddCouponButtonMenu(object sender, RoutedEventArgs e)
        {
            hideAllCouponMenu();
            Add_Coupon.Visibility = System.Windows.Visibility.Visible;
            clearFields_AddCoupon();
        }

        private void EditCouponButtonMenu(object sender, RoutedEventArgs e)
        {
            hideAllCouponMenu();
            EditCoupon.Visibility = System.Windows.Visibility.Visible;
            ecCouponName.Items.Clear();
            List<Coupon> approvedCoupons = controller.getAllCoupons();
            foreach (Coupon c in approvedCoupons)
            {
                ecCouponName.Items.Add(c.name);
            }
            clearFields_EditCoupon();
        }

        private void AddManagerMenuButton(object sender, RoutedEventArgs e)
        {
            hideAllManagerMenu();
            Add_manager.Visibility = System.Windows.Visibility.Visible;
        }

        private void EditDeleteMenuButton(object sender, RoutedEventArgs e)
        {
            hideAllManagerMenu();
            Delete_manager.Visibility = System.Windows.Visibility.Visible;
            emUsername.Items.Clear();
            List<Manager> lm = controller.getAllManagers();
            foreach (Manager m in lm)
                emUsername.Items.Add(m.username);
            clearFields_EditManager();
        }

        private void hideAllMenu()
        {
            Add_Store.Visibility = System.Windows.Visibility.Hidden;
            Edit_Coupon.Visibility = System.Windows.Visibility.Hidden;
            Edit_Manager.Visibility = System.Windows.Visibility.Hidden;
        }

        private void hideAllCouponMenu()
        {
            Add_Coupon.Visibility = System.Windows.Visibility.Hidden;
            EditCoupon.Visibility = System.Windows.Visibility.Hidden;
        }

        private void hideAllManagerMenu()
        {
            Add_manager.Visibility = System.Windows.Visibility.Hidden;
            Delete_manager.Visibility = System.Windows.Visibility.Hidden;
        }

        private void AddStoreClick(object sender, RoutedEventArgs e)
        {
            if (controller.GetStoreByUsername(asUsername.Text) != null)
            {
                MessageBoxResult error = MessageBox.Show("Store already exists", "", MessageBoxButton.OK);
                if (error == MessageBoxResult.OK)
                {
                    asUsername.Text = "";
                    return;
                }  
            }
            Category cat = (Category)Enum.Parse(typeof(Category), asCategory.Text);
            Store s = new Store(asUsername.Text,asOwner.Text,asAddress.Text,asCity.Text,asDescription.Text,cat,asName.Text,asPassword.Password,asPhone.Text,asMail.Text);
            
            controller.addStore(s);
            MessageBoxResult success = MessageBox.Show("Store was added successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                clearFields_AddStore();
            }  
        }

        private void AddCouponClick(object sender, RoutedEventArgs e)
        {
            if (controller.couponExist(acCouponName.Text) == true)
            {
                MessageBoxResult error = MessageBox.Show("Coupon already exists", "", MessageBoxButton.OK);
                if (error == MessageBoxResult.OK)
                {
                    acCouponName.Text = "";
                    return;
                }
            }
            int price, priceAfter;
            bool priceValid = Int32.TryParse(acPrice.Text, out price);
            bool priceAfterValid = Int32.TryParse(acPriceAfterDiscount.Text, out priceAfter);
            if (priceAfterValid == false || priceValid == false || priceAfter >= price)
            {
                MessageBoxResult result = MessageBox.Show("Price and Price After Discount has a conflict.", "", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    acPrice.Text = "";
                    acPriceAfterDiscount.Text = "";
                    return;
                }
            }
            DateTime today = DateTime.Today;
            today.AddDays(30);
            Category cat = (Category)Enum.Parse(typeof(Category),acCategory.Text);
            int approved = (acApproved.Text == "true") ? 1 : 0;
            Coupon c = new Coupon(acCouponName.Text, acDescription.Text, price, priceAfter, 0, 0, cat, today, acStoreName.Text, approved);

            controller.addCoupon(c);
            MessageBoxResult success = MessageBox.Show("Coupon was added successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                clearFields_AddCoupon();
            }  
        }

        private void EditCouponClick(object sender, RoutedEventArgs e)
        {
            int price, priceAfter;
            bool priceValid = Int32.TryParse(ecPrice.Text, out price);
            bool priceAfterValid = Int32.TryParse(ecPriceAfterDiscount.Text, out priceAfter);
            if (priceAfterValid == false || priceValid == false || priceAfter >= price)
            {
                MessageBoxResult result = MessageBox.Show("Price and Price After Discount has a conflict.", "", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    ecPrice.Text = "";
                    ecPriceAfterDiscount.Text = "";
                    return;
                }
            }
            Category c = (Category)Enum.Parse(typeof(Category),ecCategory.Text);
            int approved = (ecApproved.Text == "true") ? 1 : 0;
            Coupon newCoupon = new Coupon(ecCouponName.Text, ecDescription.Text, price, priceAfter, 0, 0, c, DateTime.Today, ecStoreName.Text, approved);
            bool isEdited = controller.editCoupon(ecCouponName.Text, newCoupon);
            MessageBoxResult success = MessageBox.Show("Coupon was edited successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                clearFields_EditCoupon();
            }  
        }

        private void clearFields_EditCoupon()
        {
            ecCouponName.Text = "";
            ecDescription.Text = "";
            ecPrice.Text = "";
            ecPriceAfterDiscount.Text = "";
            ecCategory.Text = "";
            ecStoreName.Text = "";
            ecApproved.Text = "";
        }

        private void clearFields_AddCoupon()
        {
            acCouponName.Text = "";
            acDescription.Text = "";
            acPrice.Text = "";
            acPriceAfterDiscount.Text = "";
            acCategory.Text = "";
            acStoreName.Text = "";
            acApproved.Text = "";
        }

        private void clearFields_AddStore()
        {
            asUsername.Text = "";
            asOwner.Text = "";
            asAddress.Text = "";
            asCity.Text = "";
            asDescription.Text = "";
            asCategory.Text = "";
            asName.Text = "";
            asPassword.Password = "";
            asPhone.Text = "";
            asMail.Text = "";
        }

        private void clearFields_AddManager()
        {
            amUsername.Text = "";
            amPassword.Password = "";
            amPhone.Text = "";
            amEmail.Text = "";
        }

        private void clearFields_EditManager()
        {
            emUsername.Text = "";
            emPassword.Password = "";
            emPhone.Text = "";
            emEmail.Text = "";
        }

        private void EditCoupon_NameChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = (sender as ComboBox).SelectedItem as string;
            if (name != null)
            {
                Coupon c = controller.getCouponByName(name);
                ecDescription.Text = c.description;
                ecPrice.Text = "" + c.price;
                ecPriceAfterDiscount.Text = "" + c.priceAfterDiscount;
                ecCategory.Text = c.category.ToString();
                ecStoreName.Text = c.storeName;
                ecApproved.Text = (c.approved) ? "true" : "false";
            }
        }

        private void DeleteCouponClick(object sender, RoutedEventArgs e)
        {
            if (ecCouponName.Text == "")
            {
                MessageBoxResult result = MessageBox.Show("Nothing to delete.", "", MessageBoxButton.OK);
                return;
            }
            bool success = controller.removeCoupon(ecCouponName.Text);
            if (success == false)
            {
                MessageBoxResult result = MessageBox.Show("Item was not removed.", "", MessageBoxButton.OK);
                return;
            }

            MessageBoxResult deleted = MessageBox.Show("Item was removed successfully.", "", MessageBoxButton.OK);
            ecCouponName.Items.Clear();
            List<Coupon> approvedCoupons = controller.getAllCoupons();
            foreach (Coupon c in approvedCoupons)
            {
                ecCouponName.Items.Add(c.name);
            }
            clearFields_EditCoupon();
        }

        private void addManagerButtonClick(object sender, RoutedEventArgs e)
        {
            if (controller.usernameExist(amUsername.Text) == true)
            {
                MessageBoxResult error = MessageBox.Show("Manager already exists", "", MessageBoxButton.OK);
                if (error == MessageBoxResult.OK)
                {
                    amUsername.Text = "";
                    return;
                }
            }
            Manager m = new Manager(amUsername.Text, amPassword.Password, amPhone.Text, amEmail.Text);

            controller.addManager(m);
            MessageBoxResult success = MessageBox.Show("Manager was added successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                clearFields_AddManager();
            }  
        }

        private void removeManagerButtonClick(object sender, RoutedEventArgs e)
        {
            if (emUsername.Text == "" || emUsername.Text == null)
            {
                MessageBoxResult error = MessageBox.Show("No manager has been selected.", "", MessageBoxButton.OK);
                if (error == MessageBoxResult.OK)
                    return;
            }
            controller.removeManager(emUsername.Text);
            MessageBoxResult success = MessageBox.Show("Manager was removed successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                clearFields_EditManager();
            }  
        }

        private void editManagerButtonClick(object sender, RoutedEventArgs e)
        {
            if (emPassword.Password == "" || emPhone.Text == "" || emEmail.Text == "")
            {
                MessageBoxResult error = MessageBox.Show("Some of the details are missing.", "", MessageBoxButton.OK);
                if (error == MessageBoxResult.OK)
                    return;
            }
            Manager m = new Manager(emUsername.Text, emPassword.Password, emPhone.Text, emEmail.Text);

            controller.editManager(emUsername.Text, m);
            MessageBoxResult success = MessageBox.Show("Manager was edited successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                clearFields_EditManager();
            }  
        }

        private void EditManager_ManagerChanged(object sender, SelectionChangedEventArgs e)
        {
            string username = (sender as ComboBox).SelectedItem as string;
            if (username != null && username != "")
            {
                Manager m = controller.GetManagerByUsername(username);
                emUsername.Text = m.username;
                emPassword.Password = m.password;
                emPhone.Text = m.phone;
                emEmail.Text = m.mail;
            }
        }
	}
}