﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace PL
{
	/// <summary>
	/// Interaction logic for StoreMenu.xaml
	/// </summary>
	public partial class StoreMenu : UserControl
	{
        private MainWindow main;
        private LoginScreen ls;
        private Store store;
        private StoreController controller;

        public StoreMenu(MainWindow main, LoginScreen ls, StoreController controller)
		{
			this.InitializeComponent();
            this.main = main;
            this.ls = ls;
            this.controller = controller;
            store = controller.GetStoreByUsername(ls.Username.Text);
            var categories = Enum.GetValues(typeof(BL_Backend.Category));
            foreach(Category c in categories){
                acCategory.Items.Add(c.ToString());
            }
            hideAll();
            Add_Coupon.Visibility = System.Windows.Visibility.Visible;
		}

		private void AddCouponButtonMenu(object sender, System.Windows.RoutedEventArgs e)
		{
            hideAll();
            Add_Coupon.Visibility = System.Windows.Visibility.Visible;
		}

		private void CouponRealizationButtonMenu(object sender, System.Windows.RoutedEventArgs e)
		{
            hideAll();
            Coupon_Realization.Visibility = System.Windows.Visibility.Visible;
		}

		private void PurchaseListButtonMenu(object sender, System.Windows.RoutedEventArgs e)
		{
            List<Order> orders = controller.myStoreOrders(store.storeName);
            if (orders.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("No orders has been made yet.", "", MessageBoxButton.OK);
                return;
            }
            main.Content = new OrderScreen(main, this, orders, store.username);
		}

		private void CouponRateButtonMenu(object sender, System.Windows.RoutedEventArgs e)
		{
            hideAll();
            Coupon_Rate.Visibility = System.Windows.Visibility.Visible;
            crCoupon.Items.Clear();
            List<Coupon> myCoupons = controller.getStoreCoupons(store.storeName);
            foreach (Coupon c in myCoupons)
            {
                crCoupon.Items.Add(c.name);
            }
		}

		private void LogOutButtonMenu(object sender, System.Windows.RoutedEventArgs e)
		{
            main.Content = ls;
		}

		private void AddCouponButton(object sender, System.Windows.RoutedEventArgs e)
		{
            bool alreadyExist = controller.couponExist(acName.Text);
            if (alreadyExist == true)
            {
                MessageBoxResult result = MessageBox.Show("Coupon with that name already exists.", "Choose other name", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    acName.Text = "";
                    return;
                }
            }
            int price, priceAfter;
            bool priceValid = Int32.TryParse(acPrice.Text, out price);
            bool priceAfterValid = Int32.TryParse(acPriceAfterDiscount.Text, out priceAfter);
            if (priceAfterValid == false || priceValid == false || priceAfter >= price)
            {
                MessageBoxResult result = MessageBox.Show("Price and Price After Discount has a conflict.", "", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    acPrice.Text = "";
                    acPriceAfterDiscount.Text = "";
                    return;
                }
            }

            DateTime expiration = DateTime.Today;
            expiration.AddDays(30);
            Category cat = (Category)Enum.Parse(typeof(Category), acCategory.Text);
            Coupon c = new Coupon(acName.Text,acDescription.Text,price,priceAfter,0,0,cat,expiration,store.storeName,0);

            controller.addCoupon(c);
            MessageBoxResult success = MessageBox.Show("Coupon was added successfully", "", MessageBoxButton.OK);
            if (success == MessageBoxResult.OK)
            {
                acName.Text = "";
                acDescription.Text = "";
                acPrice.Text = "";
                acPriceAfterDiscount.Text = "";
                acCategory.Text = "";
            }   
		}

		private void GoRealizationButton(object sender, System.Windows.RoutedEventArgs e)
		{
            string serial = serialKey.Text;
            int succeeded = controller.useCoupon(serial);
            string msg = "";
            switch (succeeded)
            {
                case 1:
                    msg = "Coupon was approved!";
                    break;
                case -1:
                    msg = "Serialkey is not valid.";
                    break;
                case -2:
                    msg = "Coupon was already used!";
                    break;
            }
            MessageBoxResult result = MessageBox.Show(msg, "", MessageBoxButton.OK);
            if (result == MessageBoxResult.OK)
            {
                serialKey.Text = "";
            }
		}

        private void hideAll()
        {
            Add_Coupon.Visibility = System.Windows.Visibility.Hidden;
            Coupon_Realization.Visibility = System.Windows.Visibility.Hidden;
            Coupon_Rate.Visibility = System.Windows.Visibility.Hidden;
        }

        private void CouponChanged(object sender, SelectionChangedEventArgs e)
        {
            string name = (sender as ComboBox).SelectedItem as string;
            List<Coupon> myCoupons = controller.getStoreCoupons(store.storeName);
            foreach (Coupon c in myCoupons)
            {
                if (c.name == name)
                {
                    crRate.Text = "" + c.rate;
                    crNumOfRaters.Text = "" + c.numOfRaters;
                    break;
                }
            }
        }
	}
}