﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using BL;
using BL_Backend;
using System.Windows.Threading;

namespace PL
{
    /// <summary>
    /// Interaction logic for UserMenu.xaml
    /// </summary>
    public partial class UserMenu : UserControl
    {
        private MainWindow main;
        private LoginScreen ls;
        private UserController controller;
        private Thread notifications;
        private SignedUser user;

        public UserMenu(MainWindow main, LoginScreen ls, UserController controller, SignedUser user)
		{
			this.InitializeComponent();
            this.main = main;
            this.ls = ls;
            this.controller = controller;
            this.user = user;
            this.notifications = new Thread(popupNotification);
            notifications.IsBackground = true;
            this.notifications.Start();
            hideAll();
            searchBusiness.Visibility = System.Windows.Visibility.Visible;
		}

        private void searchStore_Button(object sender, RoutedEventArgs e)
        {
            string storeName = (bName.Text != "") ? bName.Text : null;
            string city = (bCity.Text != "") ? bCity.Text : null ;
            List<Category> cat = createListOfStoreCategories();
            if (storeName == null && city == null && cat.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("Please fill at least one of the options.", "", MessageBoxButton.OK);
                return;
            }
            List<Store> stores = controller.searchStore(storeName, city, cat);
            clearSearchStore();
            if (stores.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("No stores matching your preferences.", "", MessageBoxButton.OK);
                return;
            }
            main.Content = new StoresScreen(main, this, stores, user.username);
        }

        private void searchCoupon_Button(object sender, RoutedEventArgs e)
        {
            string storeName = (cName.Text != "") ? cName.Text : null;
            string city = (cCity.Text != "") ? cCity.Text : null;
            List<Category> cat = createListOfCouponCategories();
            if (storeName == null && city == null && cat.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("Please fill at least one of the options.", "", MessageBoxButton.OK);
                return;
            }
            List<Coupon> coupons = controller.searchCoupon(storeName, city, cat);
            clearSearchCoupons();
            if (coupons.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("No coupons matching your preferences.", "", MessageBoxButton.OK);
                return;
            }
            this.main.Content = new CouponMenu(main, this, controller, coupons, user.username, true);
        }

        private void Logout_Button(object sender, System.Windows.RoutedEventArgs e)
        {
            main.Content = ls;
        }

        private void searchStoreClick(object sender, RoutedEventArgs e)
        {
            hideAll();
            clearSearchCoupons();
            searchBusiness.Visibility = System.Windows.Visibility.Visible;
        }

        private void searchCouponsClick(object sender, RoutedEventArgs e)
        {
            hideAll();
            clearSearchStore();
            searchCoupon.Visibility = System.Windows.Visibility.Visible;
        }

        private void myCouponsClick(object sender, RoutedEventArgs e)
        {
            List<Order> orders = controller.myCoupons(user.username);
            if (orders.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("No coupons ordered by you were found.", "", MessageBoxButton.OK);
                return;
            }
            bool almostExpired = false;
            StringBuilder sb = new StringBuilder("The following coupons are almost expired:\n");
            foreach (Order o in orders)
            {
                Coupon c = controller.getCouponByName(o.couponName);
                TimeSpan difference = c.expiration - DateTime.Today;
                if (difference.TotalDays <= 7)
                {
                    almostExpired = true;
                    sb.Append(c.name);
                    sb.Append("\n");
                }
            }
            if (almostExpired)
            {
                MessageBoxResult result = MessageBox.Show(sb.ToString(), "", MessageBoxButton.OK);
            }
            main.Content = new OrderScreen(main, this, orders, user.username);
        }

        private void hideAll()
        {
            searchBusiness.Visibility = System.Windows.Visibility.Hidden;
            searchCoupon.Visibility = System.Windows.Visibility.Hidden;
        }

        private List<Category> createListOfStoreCategories()
        {
            List<Category> categories = new List<Category>();
            if (bFood.IsChecked == true)
            {
                categories.Add(Category.Food);
            }
            if (bSport.IsChecked == true)
            {
                categories.Add(Category.Sport);
            }
            if (bElectronics.IsChecked == true)
            {
                categories.Add(Category.Electronics);
            }
            if (bCinema.IsChecked == true)
            {
                categories.Add(Category.Cinema);
            }
            if (bOutdoors.IsChecked == true)
            {
                categories.Add(Category.Outdoors);
            }
            if (bFashion.IsChecked == true)
            {
                categories.Add(Category.Fashion);
            }
            if (bToys.IsChecked == true)
            {
                categories.Add(Category.Toys);
            }
            if (bHomeAndOffice.IsChecked == true)
            {
                categories.Add(Category.HomeAndOffice);
            }
            if (bOther.IsChecked == true)
            {
                categories.Add(Category.Other);
            }
            return categories;
        }

        private List<Category> createListOfCouponCategories()
        {
            List<Category> categories = new List<Category>();
            if (cFood.IsChecked == true)
            {
                categories.Add(Category.Food);
            }
            if (cSport.IsChecked == true)
            {
                categories.Add(Category.Sport);
            }
            if (cElectronics.IsChecked == true)
            {
                categories.Add(Category.Electronics);
            }
            if (cCinema.IsChecked == true)
            {
                categories.Add(Category.Cinema);
            }
            if (cOutdoors.IsChecked == true)
            {
                categories.Add(Category.Outdoors);
            }
            if (cFashion.IsChecked == true)
            {
                categories.Add(Category.Fashion);
            }
            if (cToys.IsChecked == true)
            {
                categories.Add(Category.Toys);
            }
            if (cHomeAndOffice.IsChecked == true)
            {
                categories.Add(Category.HomeAndOffice);
            }
            if (cOther.IsChecked == true)
            {
                categories.Add(Category.Other);
            }
            return categories;
        }

        private void clearSearchStore()
        {
            bName.Text = "";
            bCity.Text = "";
            bFood.IsChecked = false;
            bSport.IsChecked = false;
            bElectronics.IsChecked = false;
            bCinema.IsChecked = false;
            bOutdoors.IsChecked = false;
            bFashion.IsChecked = false;
            bToys.IsChecked = false;
            bHomeAndOffice.IsChecked = false;
            bOther.IsChecked = false;
        }

        private void clearSearchCoupons()
        {
            cName.Text = "";
            cCity.Text = "";
            cFood.IsChecked = false;
            cSport.IsChecked = false;
            cElectronics.IsChecked = false;
            cCinema.IsChecked = false;
            cOutdoors.IsChecked = false;
            cFashion.IsChecked = false;
            cToys.IsChecked = false;
            cHomeAndOffice.IsChecked = false;
            cOther.IsChecked = false;
        }

        private void popupNotification()
        {
            while (true)
            {
                Thread.Sleep(4000); 
                //coupons popup notification
                List<Coupon> coupons = controller.searchCoupon(null, "Beer Sheva", user.categories);
                Console.WriteLine(coupons.Count);
                if (coupons.Count > 0)
                {
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        notification.Visibility = System.Windows.Visibility.Visible;
                    }));
                }
                //sleep for 10 minutes
                Thread.Sleep(600000); 
            }
        }

        private void show_button(object sender, System.Windows.RoutedEventArgs e)
        {
			List<Coupon> coupons = controller.searchCoupon(null, "Beer Sheva", user.categories);
        	main.Content = new CouponMenu(main, this, controller, coupons, user.username, true);
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                notification.Visibility = System.Windows.Visibility.Hidden;
            }));
        }

        private void cancel_button(object sender, System.Windows.RoutedEventArgs e)
        {
        	notification.Visibility = System.Windows.Visibility.Hidden;
        }

    }
}
