﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace PL
{
	/// <summary>
	/// Interaction logic for Signup.xaml
	/// </summary>
	public partial class Signup : UserControl
	{
        private MainWindow main;
        private LoginScreen ls;
        private CreateControllers controller;

        public Signup(MainWindow main, LoginScreen ls, CreateControllers controller)
		{
			this.InitializeComponent();
            this.main = main;
            this.ls = ls;
            this.controller = controller;
		}

		private void Signup_button(object sender, System.Windows.RoutedEventArgs e)
		{
            bool isExist = controller.getLoginController().usernameExist(UserName.Text);
            if (isExist == true)
            {
                MessageBoxResult result = MessageBox.Show("Username is already exists.", "Please choose another one.", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    UserName.Text = "";
                }
                return;
            }
            if(PassWord.Password != ConfirmPass.Password){
                MessageBoxResult result = MessageBox.Show("Passwords are not matching.", "", MessageBoxButton.OK);
                if (result == MessageBoxResult.OK)
                {
                    PassWord.Password = "";
                    ConfirmPass.Password = "";
                }
                return;
            }
            List<Category> categories = createListOfCategory();
            SignedUser su = new SignedUser(UserName.Text,"",PassWord.Password,Phone.Text,Mail.Text);
            foreach (Category c in categories)
            {
                su.addCategory(c);
            }
            controller.getLoginController().addSignedUser(su);
            main.Content = new UserMenu(main, ls, controller.getUserController(),su);
		}

		private void Back_button(object sender, System.Windows.RoutedEventArgs e)
		{
            main.Content = ls;
		}

        private List<Category> createListOfCategory()
        {
            List<Category> categories = new List<Category>();
            if (Food.IsChecked == true)
            {
                categories.Add(Category.Food);
            }
            if (Sport.IsChecked == true)
            {
                categories.Add(Category.Sport);
            }
            if (Electronics.IsChecked == true)
            {
                categories.Add(Category.Electronics);
            }
            if (Cinema.IsChecked == true)
            {
                categories.Add(Category.Cinema);
            }
            if (Outdoors.IsChecked == true)
            {
                categories.Add(Category.Outdoors);
            }
            if (Fashion.IsChecked == true)
            {
                categories.Add(Category.Fashion);
            }
            if (Toys.IsChecked == true)
            {
                categories.Add(Category.Toys);
            }
            if (Home.IsChecked == true)
            {
                categories.Add(Category.HomeAndOffice);
            }
            if (Other.IsChecked == true)
            {
                categories.Add(Category.Other);
            }
            return categories;
        }
	}
}