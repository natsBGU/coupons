﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace PL
{
	/// <summary>
	/// Interaction logic for OrderMenu.xaml
	/// </summary>
	public partial class OrderScreen : UserControl
	{
        private MainWindow main;
        private UserControl backScreen;
        private Order[] Orders;
        private int currentIndex;
        private int size;
        private string username;

        public OrderScreen(MainWindow main, UserControl backScreen, List<Order> Orders, string username)
		{
			this.InitializeComponent();
            this.main = main;
            this.backScreen = backScreen;
            this.Orders = Orders.ToArray();
            this.currentIndex = 0;
            this.size = Orders.Count;
            this.username = username;
            if (size > 0)
            {
                displayOrder(Orders[0]);
                updateIsEnable();
            }
		}
        private void prevButtonClick(object sender, RoutedEventArgs e)
        {
            currentIndex--;
            displayOrder(Orders[currentIndex]);
            updateIsEnable();
        }

        private void nextButtonClick(object sender, RoutedEventArgs e)
        {
            currentIndex++;
            displayOrder(Orders[currentIndex]);
            updateIsEnable();
        }

        private void backButtonClick(object sender, RoutedEventArgs e)
        {
            this.main.Content = backScreen;
        }

        private void updateIsEnable()
        {
            this.prevButton.IsEnabled = true;
            this.nextButton.IsEnabled = true;
            if (currentIndex == 0)
            {
                this.prevButton.IsEnabled = false;
            }
            if (currentIndex == size-1)
            {
                this.nextButton.IsEnabled = false;
            }
        }

        private void displayOrder(Order o)
        {
            couponName.Text = o.couponName;
            serial.Text = o.serial;
            purchaseDate.Text = o.orderDate.ToShortDateString();
            quantity.Text = "" + o.quantity;
            isUsed.Text = (o.isUsed) ? "yes" : "no";
        }

	}
}