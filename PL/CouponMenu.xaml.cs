﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace PL
{
	/// <summary>
	/// Interaction logic for CouponMenu.xaml
	/// </summary>
	public partial class CouponMenu : UserControl
	{
        private MainWindow main;
        private UserMenu user;
        private UserController controller;
        private Coupon[] coupons;
        private int currentIndex;
        private int size;
        private string username;

        public CouponMenu(MainWindow main, UserMenu user, UserController controller, List<Coupon> coupons, string username, bool orderMode)
		{
			this.InitializeComponent();
            this.main = main;
            this.user = user;
            this.controller = controller;
            this.coupons = coupons.ToArray();
            this.currentIndex = 0;
            this.size = coupons.Count;
            this.username = username;
            if (size > 0)
            {
                displayCoupon(coupons[0]);
                updateIsEnable();
            }
            this.orderButton.Visibility = (orderMode == true) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
		}
        private void prevButtonClick(object sender, RoutedEventArgs e)
        {
            currentIndex--;
            displayCoupon(coupons[currentIndex]);
            updateIsEnable();
        }

        private void nextButtonClick(object sender, RoutedEventArgs e)
        {
            currentIndex++;
            displayCoupon(coupons[currentIndex]);
            updateIsEnable();
        }

        private void orderButtonClick(object sender, RoutedEventArgs e)
        {
            Coupon c = coupons[currentIndex];
            controller.orderCoupon(c, 1, username);
            MessageBoxResult success = MessageBox.Show("Coupon was ordered successfully", "", MessageBoxButton.OK);
        }

        private void backButtonClick(object sender, RoutedEventArgs e)
        {
            this.main.Content = user;
        }

        private void updateIsEnable()
        {
            this.prevButton.IsEnabled = true;
            this.nextButton.IsEnabled = true;
            if (currentIndex == 0)
            {
                this.prevButton.IsEnabled = false;
            }
            if (currentIndex == size-1)
            {
                this.nextButton.IsEnabled = false;
            }
        }

        private void displayCoupon(Coupon c)
        {
            couponName.Text = c.name;
            description.Text = c.description;
            price.Text = "" + c.price;
            priceAfterDiscount.Text = "" + c.priceAfterDiscount;
            storeName.Text = c.storeName;
            category.Text = c.category.ToString();
            rating.Text = "" + c.rate;
            numOfRaters.Text = "" + c.numOfRaters;
        }

	}
}