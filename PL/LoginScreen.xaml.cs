﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;
using DAL;

namespace PL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginScreen : UserControl
    {
        private MainWindow main;
        private CreateControllers controller;

        public LoginScreen(MainWindow main, CreateControllers controller)
        {
            InitializeComponent();
            this.main = main;
            this.controller = controller;
        }

        private void Login_button(object sender, System.Windows.RoutedEventArgs e)
        {
            int clearence = controller.getLoginController().login(Username.Text, Password.Password);
            Password.Password = "";
            switch (clearence)
            {
                case 0:
                    MangerMenu manager = new MangerMenu(main, this, controller.getManagerController());
                    main.Content = manager;
                    break;

                case 1:
                    StoreMenu store = new StoreMenu(main, this, controller.getStoreController());
                    main.Content = store;
                    break;
                
                case 2:
                    SignedUser su = controller.getLoginController().GetSignedUserByUsername(Username.Text);
                    UserMenu user = new UserMenu(main, this, controller.getUserController(),su);
                    main.Content = user;
                    break;

                default:
                    MessageBoxResult result = MessageBox.Show("Username or password are incorrect.", "",MessageBoxButton.OK);
                    if (result == MessageBoxResult.OK)
                    {
                        Username.Text = "";
                    }
                    break;
            }
        }

        private void SignUp_button(object sender, System.Windows.RoutedEventArgs e)
        {
            Username.Text = "";
            Password.Password = "";
            Signup signup = new Signup(main, this, controller);
            main.Content = signup;
		}

        private void Forget_button(object sender, System.Windows.RoutedEventArgs e)
        {
            forgetPass.Visibility = System.Windows.Visibility.Visible;
        }

        private void send_button(object sender, System.Windows.RoutedEventArgs e)
        {
            string email = controller.getLoginController().getSignUserEmail(usernameForget.Text);
            if (email != "")
            {
                string password = controller.getLoginController().getSignUserPassword(usernameForget.Text);
                string to = email;
                string subject = "Coupon user password";
                string body = "Your password is " + password + "\n\nYours,\nCoupon's Family (;\n";
                MailHandler.sendMail(email,subject,body);
                MessageBox.Show("The mail was sent.\nPlease check your email.", "Success", MessageBoxButton.OK);
            }
            usernameForget.Text = "";
            forgetPass.Visibility = System.Windows.Visibility.Hidden;    

        }

        private void cancel_button(object sender, System.Windows.RoutedEventArgs e)
        {
            forgetPass.Visibility = System.Windows.Visibility.Hidden;
        }

    }
}
